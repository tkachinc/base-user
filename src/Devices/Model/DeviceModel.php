<?php
namespace TkachInc\BaseUser\Devices\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * Class DeviceModel
 *
 * @property string userId
 * @package TkachInc\Core\User\Devices\Model
 */
abstract class DeviceModel extends ObjectModel
{
	protected static $_pk = '_id';

	protected static $_sort = ['time' => -1];

	protected static $_writeConcernMode = 1;

	protected static $_readPreference = ObjectModel::RP_PRIMARY_PREFERRED;

	protected static $_indexes = [
		[
			'keys' => ['advertisingId' => 1],
		],
		[
			'keys' => ['pushToken' => 1],
		],
		[
			'keys' => ['userId' => 1],
		],
	];

	protected static $_fieldsDefault = [
		'_id'           => '',
		'userId'        => '',
		'pushToken'     => '',
		'advertisingId' => '',
		'time'          => 0,
	];

	protected static $_fieldsValidate = [
		'_id'           => self::TYPE_MONGO_ID,
		'userId'        => self::TYPE_STRING,
		'pushToken'     => self::TYPE_STRING,
		'advertisingId' => self::TYPE_STRING,
		'time'          => self::TYPE_TIMESTAMP,
	];

	protected static $_fieldsPrivate = [
		'_id' => 1,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;

	protected static $_updateMethod = self::UPDATE_METHOD_SET;

	protected static $_hasPrefix = true;
}
