<?php
namespace TkachInc\BaseUser\Devices\Model;

/**
 * Class DeviceModel
 *
 * @property string advertisingId
 * @property string deviceId
 * @property int    time
 * @package TkachInc\Core\User\Devices\Model
 */
class DeviceGooglePlay extends DeviceModel
{
	protected static $_collection = 'android_devices';

	protected static $_indexes = [
		[
			'keys' => ['advertisingId' => 1],
		],
		[
			'keys' => ['pushToken' => 1],
		],
		[
			'keys' => ['userId' => 1],
		],
		[
			'keys' => ['deviceId' => 1],
		],
		[
			'keys' => ['time' => -1],
		],
	];

	protected static $_fieldsDefault = [
		'deviceId'      => '',
		'deviceRawData' => [],
	];

	protected static $_fieldsValidate = [
		'deviceId'      => self::TYPE_STRING,
		'deviceRawData' => self::TYPE_JSON,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;

	protected static $_updateMethod = self::UPDATE_METHOD_SET;

	protected static $_hasPrefix = true;
}