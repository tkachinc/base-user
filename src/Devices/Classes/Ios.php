<?php
namespace TkachInc\BaseUser\Devices\Classes;

use TkachInc\BaseUser\Devices\Model\DeviceIos;
use TkachInc\BaseUser\Devices\Model\DeviceModel;
use TkachInc\BaseUser\Model\UserModel;
use TkachInc\Engine\Services\Helpers\Generator\HashGenerator;

/**
 * @property int pushToken
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Ios implements IDevice
{
	protected $vendorId;

	protected $advertisingId;

	/**
	 * Android constructor.
	 *
	 * @param $vendorId
	 * @param $advertisingId
	 */
	public function __construct($vendorId, $advertisingId)
	{
		$this->vendorId = $vendorId;
		$this->advertisingId = $advertisingId;
	}

	/**
	 * @param UserModel $user
	 *
	 * @return DeviceModel
	 */
	public function login(UserModel $user):DeviceModel
	{
		$device = new DeviceIos(
			['$or' => [['advertisingId' => $this->advertisingId], ['vendorId' => $this->vendorId]]]
		);
		if (!$device->isLoadedObject()) {
			if ($user->isLoadedObject()) {
				$device->userId = $user->_id;
			} else {
				$device->userId = HashGenerator::getStringUniqueId();
			}

			if ($this->advertisingId) {
				$device->advertisingId = $this->advertisingId;
			}
			if ($this->vendorId) {
				$device->deviceId = $this->vendorId;
			}
		}
		$device->time = time();

		return $device;
	}
}