<?php
namespace TkachInc\BaseUser\Devices\Classes;

use TkachInc\BaseUser\Devices\Model\DeviceModel;
use TkachInc\BaseUser\Devices\Model\DeviceGooglePlay;
use TkachInc\BaseUser\Model\UserModel;
use TkachInc\Engine\Services\Helpers\Generator\HashGenerator;

/**
 * @property int pushToken
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class GooglePlay implements IDevice
{
	protected $deviceId;

	protected $advertisingId;

	/**
	 * Android constructor.
	 *
	 * @param $deviceId
	 * @param $advertisingId
	 */
	public function __construct($deviceId, $advertisingId)
	{
		$this->deviceId = $deviceId;
		$this->advertisingId = $advertisingId;
	}

	/**
	 * @param UserModel $user
	 *
	 * @return DeviceModel
	 */
	public function login(UserModel $user):DeviceModel
	{
		$device = new DeviceGooglePlay(
			['$or' => [['advertisingId' => $this->advertisingId], ['deviceId' => $this->deviceId]]]
		);
		if (!$device->isLoadedObject()) {
			if ($user->isLoadedObject()) {
				$device->userId = $user->_id;
			} else {
				$device->userId = HashGenerator::getStringUniqueId();
			}

			if ($this->advertisingId) {
				$device->advertisingId = $this->advertisingId;
			}
			if ($this->deviceId) {
				$device->deviceId = $this->deviceId;
			}
		}
		$device->time = time();

		return $device;
	}
}