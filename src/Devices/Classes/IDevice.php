<?php
namespace TkachInc\BaseUser\Devices\Classes;

use TkachInc\BaseUser\Devices\Model\DeviceModel;
use TkachInc\BaseUser\Model\UserModel;

/**
 * Class IDevice
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
interface IDevice
{
	/**
	 * @param UserModel $user
	 *
	 * @return DeviceModel
	 */
	public function login(UserModel $user): DeviceModel;
}