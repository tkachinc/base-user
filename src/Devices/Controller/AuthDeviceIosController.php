<?php
namespace TkachInc\BaseUser\Devices\Controller;

use TkachInc\BaseUser\Controller\AuthController;
use TkachInc\BaseUser\Devices\Classes\Ios;
use TkachInc\BaseUser\Model\UserModel;
use TkachInc\Engine\Services\Request\Request;

/**
 * Class DeviceController
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AuthDeviceIosController extends AuthController
{
	public function login()
	{
		$user = new UserModel(Request::getPost('userId', null));
		$vendorId = Request::getPost('vendorId', '');
		$advertisingId = Request::getPost('advertisingId', '');
		if (empty($deviceId) && empty($advertisingId)) {
			$this->code = 404;
			throw new \Exception('Not found deviceId and advertisingId');
		}

		$device = new Ios($vendorId, $advertisingId);
		$deviceModel = $device->login($user);
		$deviceModel->save();
		$this->application->login($this->dateTime, $deviceModel->userId, $user);
	}

	public function pushToken()
	{
		$user = new UserModel(Request::getPost('userId', null));
		$pushToken = Request::getPost('pushToken', '');
		if (empty($pushToken)) {
			$this->code = 404;
			throw new \Exception('Not found pushToken');
		}

		$vendorId = Request::getPost('vendorId', '');
		$advertisingId = Request::getPost('advertisingId', '');
		if (empty($vendorId) && empty($advertisingId)) {
			$this->code = 404;
			throw new \Exception('Not found deviceId and advertisingId');
		}

		$device = new Ios($vendorId, $advertisingId);
		$deviceModel = $device->login($user);
		$deviceModel->pushToken = $pushToken;
		$deviceModel->save();

		$this->application->setData([]);
		$this->application->response();
	}
}