<?php
namespace TkachInc\BaseUser\Devices\Controller;

use TkachInc\BaseUser\Controller\AuthController;
use TkachInc\BaseUser\Devices\Classes\GooglePlay;
use TkachInc\BaseUser\Model\UserModel;
use TkachInc\Engine\Services\Request\Request;

/**
 * Class DeviceController
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AuthDeviceGooglePlayController extends AuthController
{
	public function login()
	{
		$user = new UserModel(Request::getPost('userId', null));
		$deviceId = Request::getPost('deviceId', '');
		$advertisingId = Request::getPost('advertisingId', '');
		$deviceRawData = Request::getPost('deviceRawData', []);
		if (empty($deviceId) && empty($advertisingId)) {
			$this->code = 404;
			throw new \Exception('Not found deviceId and advertisingId');
		}

		$device = new GooglePlay($deviceId, $advertisingId);
		$deviceModel = $device->login($user);
		$device->deviceRawData = $deviceRawData;
		$deviceModel->save();
		$this->application->login($this->dateTime, $deviceModel->userId, $user);
	}

	public function pushToken()
	{
		$user = new UserModel(Request::getPost('userId', null));
		$deviceRawData = Request::getPost('deviceRawData', []);
		$pushToken = Request::getPost('pushToken', '');
		if (empty($pushToken)) {
			$this->code = 404;
			throw new \Exception('Not found pushToken');
		}

		$deviceId = Request::getPost('deviceId', '');
		$advertisingId = Request::getPost('advertisingId', '');
		if (empty($deviceId) && empty($advertisingId)) {
			$this->code = 404;
			throw new \Exception('Not found deviceId and advertisingId');
		}

		$device = new GooglePlay($deviceId, $advertisingId);
		$deviceModel = $device->login($user);
		$deviceModel->deviceRawData = $deviceRawData;
		$deviceModel->pushToken = $pushToken;
		$deviceModel->save();

		$this->application->setData([]);
		$this->application->response();
	}
}