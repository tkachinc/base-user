<?php
namespace TkachInc\BaseUser\Controller;

use Pimple\Container;
use TkachInc\BaseUser\Model\UserModel;
use TkachInc\Engine\Application\HTTP\Classes\APIApplication;
use TkachInc\Engine\Application\ServerEvents;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\Engine\Services\Response\Response;
use TkachInc\Engine\Services\Response\ResponseTypes\SpecificJSONResponse;

/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 2/1/17
 * Time: 00:42
 */
class AuthApplication extends APIApplication
{

	public function listen(ServerEvents $listen)
	{
		// TODO: Implement listen() method.
	}

	public function register(Container $pimple)
	{
		// TODO: Implement register() method.
		$pimple[Response::class] = function () {
			return new SpecificJSONResponse();
		};
	}

	/**
	 * @param \DateTime $dateTime
	 * @param string    $userId
	 * @param UserModel $user
	 */
	public function login(\DateTime $dateTime, string $userId, UserModel $user)
	{
		if ($user->loginTime < strtotime('today')) {
			if ($user->loginTime >= strtotime('yesterday') || $user->loginTime == 0) {
				$user->dayInRow++;
			} else {
				$user->dayInRow = 0;
			}
			$user->dayIn++;
		}
		// Доп. действия при каждом логине. получения актуальных данных
		$user->loginCount++;
		$user->loginTime = $dateTime->getTimestamp();
		$user->loginIp = Request::getUserHostAddress();

		$user->save();

		$this->code = 200;
		$this->data = [];
		$this->response();
	}
}