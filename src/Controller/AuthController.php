<?php
namespace TkachInc\BaseUser\Controller;

use TkachInc\BaseUser\Model\SocUserModel;
use TkachInc\BaseUser\Model\UserModel;
use TkachInc\Engine\Application\BaseController;
use TkachInc\Engine\Services\Helpers\Generator\HashGenerator;
use TkachInc\Engine\Services\Request\Request;

/**
 * Class AuthController
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class AuthController extends BaseController
{
	/**
	 * @var AuthApplication
	 */
	protected $application;

	/**
	 * @var \DateTime
	 */
	protected $dateTime;

	/**
	 * ApiController constructor.
	 */
	public function __construct()
	{
		parent::__construct(new AuthApplication());

		$this->dateTime = new \DateTime();
	}

	public function social()
	{
		$user = new UserModel(Request::getPost('userId', null));
		$netId = Request::getPost('netId', null);
		$socId = Request::getPost('socId', null);
		$socUser = new SocUserModel(['netId' => $netId, 'socId' => $socId]);
		if (!$socUser->isLoadedObject()) {
			if ($user->isLoadedObject()) {
				$socUser->userId = $user->_id;
			} else {
				$socUser->userId = HashGenerator::getStringUniqueId();
			}
			$socUser->netId = $netId;
			$socUser->socId = $socId;
			$socUser->save();
		}
		$this->application->login($this->dateTime, $socUser->userId, $user);
	}
}