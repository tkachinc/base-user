<?php
namespace TkachInc\BaseUser\Model;

use TkachInc\Core\Database\Redis\ObjectModel;

/**
 * @property mixed  balance
 * @property mixed  sessionId
 * @property mixed  managerId
 * @property mixed  gameId
 * @property int    stack
 * @property bool   autobuyin
 * @property int    pos
 * @property string server
 * @property int    timerToWatcher
 * @property array  payload
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class UserCacheModel extends ObjectModel
{
	protected static $_separator = ':';

	protected static $_keyName = 'cache_user';

	protected static $_fieldsDefault = [
		'sessionId'      => '',
		'balance'        => 0,
		'managerId'      => '',
		'gameId'         => '',
		'stack'          => 0,
		'pos'            => '',
		'autobuyin'      => false,
		'active'      => false,
		'payload'        => [],
		'timerToWatcher' => 0,
		'server'         => '',
		'state'          => '',
	];

	protected static $_fieldsValidate = [
		'sessionId'      => self::TYPE_STRING,
		'balance'        => self::TYPE_UNSIGNED_INT,
		'managerId'      => self::TYPE_STRING,
		'gameId'         => self::TYPE_STRING,
		'stack'          => self::TYPE_UNSIGNED_INT,
		'pos'            => self::TYPE_STRING,
		'autobuyin'      => self::TYPE_BOOL,
		'active'      => self::TYPE_BOOL,
		'payload'        => self::TYPE_JSON,
		'timerToWatcher' => self::TYPE_TIMESTAMP,
		'server'         => self::TYPE_STRING,
		'state'          => self::TYPE_STRING,
	];

	protected static $_fieldsPrivate = ['sessionId' => 1];
} 