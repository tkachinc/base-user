<?php
namespace TkachInc\BaseUser\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * Class UserModel
 *
 * @property string _id
 * @property string loginCount
 * @property int    rnd
 * @property string installIp
 * @property int    registrationTime
 * @property int    loginTime
 * @property int    dayInRow
 * @property int    dayIn
 * @property int    balance
 * @property string loginIp
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class UserModel extends ObjectModel
{
	protected static $_collection = 'users';

	protected static $_pk = '_id';

	protected static $_sort = ['_id' => 1];

	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
	];

	protected static $_fieldsDefault = [
		'_id'              => '',
		'loginTime'        => 0,
		'registrationTime' => 0,
		'loginCount'       => 0,
		'dayIn'            => 0,
		'dayInRow'         => 0,
		'splits'           => [],

		'installIp' => '',
		'loginIp'   => '',
		'rnd'       => 0,
		'balance'   => 0,

		'photo'     => '',
		'nickname'  => '',
		'lastName'  => '',
		'firstName' => '',
		'age'       => '',
		'gender'    => '',

		'city'    => '',
		'country' => '',
		'birthY'  => 0,
		'birthM'  => 0,
		'birthD'  => 0,

	];

	protected static $_fieldsValidate = [
		'_id'              => self::TYPE_STRING,
		'loginTime'        => self::TYPE_TIMESTAMP,
		'registrationTime' => self::TYPE_TIMESTAMP,
		'loginCount'       => self::TYPE_UNSIGNED_INT,
		'dayIn'            => self::TYPE_UNSIGNED_INT,
		'dayInRow'         => self::TYPE_UNSIGNED_INT,
		'splits'           => self::TYPE_JSON,

		'installIp' => self::TYPE_STRING,
		'loginIp'   => self::TYPE_STRING,
		'rnd'       => self::TYPE_UNSIGNED_INT,
		'balance'   => self::TYPE_UNSIGNED_INT,

		'photo'     => self::TYPE_STRING,
		'nickname'  => self::TYPE_STRING,
		'lastName'  => self::TYPE_STRING,
		'firstName' => self::TYPE_STRING,
		'age'       => self::TYPE_STRING,
		'gender'    => self::TYPE_STRING,

		'city'    => self::TYPE_STRING,
		'country' => self::TYPE_STRING,
		'birthY'  => self::TYPE_UNSIGNED_INT,
		'birthM'  => self::TYPE_UNSIGNED_INT,
		'birthD'  => self::TYPE_UNSIGNED_INT,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;

	protected static $_fieldsPrivate = [
		'loginTime'        => 1,
		'registrationTime' => 1,
		'loginCount'       => 1,
		'dayIn'            => 1,
		'dayInRow'         => 1,
		'installIp'        => 1,
		'loginIp'          => 1,
		'rnd'              => 1,
		'lastName'         => 1,
		'firstName'        => 1,
	];
}