<?php
namespace TkachInc\BaseUser\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * Class User
 *
 * @property string userId
 * @property mixed  netId
 * @property mixed  socId
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SocUserModel extends ObjectModel
{
	protected static $_collection = 'soc_user_model';

	protected static $_pk = '_id';

	protected static $_indexes = [
		[
			'keys'   => ['socId' => 1, 'netId' => 1],
			'unique' => true,
		],
		[
			'keys' => ['userId' => 1],
		],
	];

	protected static $_fieldsDefault = [
		'_id'    => '',
		'userId' => '',
		'socId'  => '',
		'netId'  => '',

		'photo'     => '',
		'nickname'  => '',
		'lastName'  => '',
		'firstName' => '',
		'age'       => '',
		'gender'    => '',

		'city'    => '',
		'country' => '',
		'birthY'  => 0,
		'birthM'  => 0,
		'birthD'  => 0,
	];

	protected static $_fieldsValidate = [
		'_id'    => self::TYPE_STRING,
		'userId' => self::TYPE_STRING,
		'socId'  => self::TYPE_STRING,
		'netId'  => self::TYPE_STRING,

		'photo'     => self::TYPE_STRING,
		'nickname'  => self::TYPE_STRING,
		'lastName'  => self::TYPE_STRING,
		'firstName' => self::TYPE_STRING,
		'age'       => self::TYPE_STRING,
		'gender'    => self::TYPE_STRING,

		'city'    => self::TYPE_STRING,
		'country' => self::TYPE_STRING,
		'birthY'  => self::TYPE_UNSIGNED_INT,
		'birthM'  => self::TYPE_UNSIGNED_INT,
		'birthD'  => self::TYPE_UNSIGNED_INT,
	];
}